# FIDO - Anmeldeverfahren ohne Passwörter

Hören Sie die Audiodatei (14 min) und beantworten Sie parallel dazu die Fragen, die Sie in der Text-Datei finden. Die verlangte Zeichnung machen Sie von Hand auf ein weisses Papier und fotografieren Sie die Zeichnung für die Abgabe.
<br>[**Fragen** zu FIDO-Anmeldeverfahren-ohne-Passwoerter](./FIDO-Anmeldeverfahren-ohne-Passwoerter.txt)
<br>[(14 min) FIDO-Anmeldeverfahren-ohne-Passwoerter.mp3](./FIDO-Anmeldeverfahren-ohne-Passwoerter.mp3)



<br>
<br><sup>Ausschnitt aus Podcast "SRF-Digital" des Schweizer Radio am 12.1.2024</sup>
