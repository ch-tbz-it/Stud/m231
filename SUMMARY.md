# Summary

## Themen
 - [Datenschutz](/01_protection/README.md)
 - [GIT/Markdown](/02_git/README.md)
 - [Passwortverwaltung](/03_passwords/README.md)
 - [Ablagekonzept](/04_filing_system/README.md)
 - [Backup](/05_backup/README.md)

## Administratives
 - [Kompetenzmatrix](00_kompetenzband/README.md)
 - [Leistungsbeurteilung](/00_evaluation/README.md)
