# Leseauftrag - EDÖB - Datenschutz Informationsdossier

<br>

| Arbeitsauftrag  | 1  |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  bis LB1 |
| Ziel | Wichtigste Grundlagen zum Thema Datenschutz lernen |

[Der Eidgenössische Datenschutz- und Öffentlichkeitsbeauftragte (EDÖB), Quelle: Bundeskanzlei](https://www.bk.admin.ch/bk/de/home/bk/organisation-der-bundeskanzlei/eidgenossischer-datenschutz-und-offentlichkeitsbeauftragter-edob.html) stellt Lehrmittel zum Thema Datenschutz zur Verfügung. Die Lehrmittel geben einen umfassenden Einblick in die Bedeutung des Datenschutzes im Umgang mit den neuen Medien. Es hat zum Ziel Lernende für eine verantwortungsvolle Verwendung von Personendaten zum Schutz ihrer Privatsphäre und die Rücksicht auf die Persönlichkeit zu sensibilisieren. 

Zum Einstieg: Schauen und hören Sie sich den Bericht [Überwachungskameras in Mietshäusern nicht in jedem Fall erlaubt](https://www.srf.ch/news/schweiz/schweiz-ueberwachungskameras-in-mietshaeusern-nicht-in-jedem-fall-erlaubt) (18.04.2016, Radio SRF) an.

Das [**Informationsdossier**](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_basismodul.pdf.download.pdf/Lehrmittel%20Basismodul%20DE.pdf) ist zu Beginn dieses IT-Moduls (M231) in drei Abschnitten als Hausaufgabe zu lesen. Der Inhalt des Dossiers ist Teil der Leistungsbeurteilung.

 - **Teil 1**: "Was ist Datenschutz?" <br>(&rArr; bis und mit Seite 9)
 - **Teil 2**: "Moderne Informationstechnologie und ihre Risiken von Internet bis Videotelefonie" <br>(&rArr; bis und mit Seite 23)
 - **Teil 3**: "Moderne Informationstechnologie und ihre Risiken - ab Bilder und Bildrechte" <br>(&rArr; bis und mit Seite 33)

**Tipp:** Sie können für die Leistungsbeurteilung eine Zusammenfassung mitbringen. Fassen Sie das Gelesene laufend zusammen. Das unterstützt das Verständnis und dient automatisch als Prüfungsvorbereitung. 

<br>
<br>

| Arbeitsauftrag  | 2  |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Hör- und Leseauftrag mit Fragenbeantwortung |
| Zeitbudget  |  bis LB1 |
| Ziel | Wer ist und was macht der EDÖB? |


**Auftrag:** Lesen und hören Sie diese Beiträge und beantworten Sie diese Fragen. Sie schreiben alle die Fragen und Ihre Antworten in Ihr Lernjournal/Dossier (als Datei "derEdoeb.md" in GitLab)

Hier die [Fragen-Datei](media/derEdoeb.txt)

(geschätzter Zeitbedarf, 45-75 min)


- [Adrian Lobsiger, der Beauftragte seit 2016](https://www.edoeb.admin.ch/edoeb/de/home/deredoeb/derbeauftragte.html)
- [2017-06-26, 1:23, SRF TV, Datenschützer Adrian Lopsiger - Seine Herausforderungen und Erfolge](https://www.srf.ch/news/schweiz/datenschuetzer-lobsiger-seine-herausforderungen-seine-erfolge)
- [2020-06-30, 1:55, SRF TV, Das sind die Baustellen des obersten Datenschützers](https://www.srf.ch/news/schweiz/von-tiktok-bis-swiss-covid-app-das-sind-die-baustellen-des-obersten-datenschuetzers)
- [2023-08-12, Tagesanzeiger Interview "Wir brauchen diese neuen Instrumente, um die Privatsphäre zu schützen"](https://www.tagesanzeiger.ch/wir-brauchen-diese-neuen-instrumente-um-die-privatsphaere-zu-schuetzen-963165640692)
- [2023-08-12, Tagesanzeiger - So werden die Rechte der Nutzer gestärkt](https://www.tagesanzeiger.ch/so-werden-die-rechte-der-nutzerinnen-und-nutzer-gestaerkt-751776426589)
- [2023-12-20, Wahl des Datenschutz- und Öffentlichkeitsbeauftragten für die Amtsperiode 2024-2027](https://www.parlament.ch/de/ratsbetrieb/amtliches-bulletin/amtliches-bulletin-die-verhandlungen?SubjectId=63090)
- [2024-06-24, 26:20, SRF Radio, "Ich bettle um Auskunft"](media/2024-06-24_Radio-SRF-Tagesgespraech-Adrian-Lopsiger-EDOEP.mp3)

<br>

## Unterlagen

Weitere Unterlagen finden Sie auf der Seite des EDÖB und kann heruntergeladen werden. 

Das Dokument ist im Abschnitt "[Lehrmittel Datenschutz](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_basismodul.pdf.download.pdf/Lehrmittel%20Basismodul%20DE.pdf) (gültig bis 31.08.2023)" zu finden. Es basiert auf dem Datenschutzgesetz, welches bis August 2023 in Kraft war. Ob und wann ein aktualisiertes Lehrmittel vom EDÖB veröffentlicht wird, ist derzeit nicht bekannt.

- [Lehrmittel Datenschutz Sek II](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_sek2.pdf.download.pdf/Lehrmittel%20Lektionen%201-7%20Sek%20II%20DE.PDF.pdf)

- [Infothek Datenschutz EDÖB](https://www.edoeb.admin.ch/edoeb/de/home/deredoeb/infothek/infothek-ds.html) (&rArr; ganz nach unten scrollen)

## DSG/DSV

- [Datenschutzgesetz, DSG](https://www.fedlex.admin.ch/eli/fga/2022/1561/de)
- [Datenschutzverordnung, DSV](https://www.fedlex.admin.ch/eli/cc/2022/568/de)

- [Datenschutz Informationsdossier](https://www.edoeb.admin.ch/dam/edoeb/de/Dokumente/datenschutz/lehrmittel_datenschutz_basismodul.pdf.download.pdf/Lehrmittel%20Basismodul%20DE.pdf)
