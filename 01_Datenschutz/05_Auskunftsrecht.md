# Auskunft über eigene Personendaten verlangen
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Öffentliche Webseiten zum Thema Auskunftsrecht besuchen und Musterbrief studieren |
| Zeitbudget  |  1 bis 2 Lektionen |
| Ziel | Jeder Lernende weiss wo er Informationen zu seinem Auskunftsrecht findet und kennt das Verfahren  |

## Aufgabenstellung
 - Besuchen Sie die beiden Webseiten https://www.datenschutz.ch/meine-rechte-einfordern und https://www.edoeb.admin.ch/edoeb/de/home/datenschutz/grundlagen/auskunftsrecht.html
 - Angenommen Sie wollen wissen, welche Personendaten Ihr Mobilfunkanbieter über Sie gespeichert hat. **Wie müssen Sie vorgehen?**
 - Laden Sie sich den Musterbrief herunter und füllen Sie diesen Proforma aus. 
   - Eigene Angaben einfüllen
   - Adresse des Mobilfunkanbieters einfügen: Wo ist die korrekte Adresse der Firma zu finden? (Stichwort: Zefix)
 - Das ausgefüllte Auskunftsbegehren (OHNE ID KOPIE und OHNE Unterschrift) als PDF an die Lehrperson schicken (Abgabemodalität bei der Lehrperson erfragen). 

## Hinweis
Diese Aufgabe ist Teil der [Leistungsbeurteilung LB3](../99_Leistungsbeurteilung/). 

## Quellen
 - https://www.edoeb.admin.ch/edoeb/de/home/datenschutz/dokumentation/musterbriefe/allgemeine-auskunfts---loeschungs--und-berichtigungsbegehren.html
 - https://www.datenschutz.ch/meine-rechte-einfordern
 - https://www.edoeb.admin.ch/edoeb/de/home/dokumentation/datenschutz/musterbriefe/wie-sie-vorgehen--wenn-sie-ein-auskunftsbegehren-stellen-moechte.html

