# Challange - Eigene Verschlüsselung

| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Klassenchallenge |
| Aufgabenstellung  | Eigene Verschlüsselung entwickeln |
| Zeitbudget  |  1.5 Lektion |
| Ziel | Herausforderung Verschlüsselung |

Wer kann die beste *"Hand-"* Verschlüsselung entwickeln?

Entwickeln Sie ein eigenes kryptografisches Verfahren, dass ohne elektronische Hilfsmittel auskommt und mit Stift und Papier verwendet werden kann. Ihr Verfahren muss in der Lage sein Buchstaben und Zahlen zu verschlüsseln. Das sicherste kryptografische Verfahren gewinnt. 

## Vorgehen
1. **5'** **Gruppenbildung**: 2 bis 4 Personen pro Gruppe
2. **20'** **Verfahren entwickeln**. Sie haben 40 Minuten Zeit um eine Verfahren zu entwickeln. Wichtig: Das Verfahren muss in der Lage sein zu verschlüsseln und auch wieder zu entschlüsseln!
3. **5'** **Nachricht verschlüsseln** Sie erhalten von der Lehrperson einen Zettel mit einer Nachricht. Verschlüsseln Sie diese Nachricht. Schreiben Sie die verschlüsselte Nachricht auf einen Zettel. 
4. **20'** Die Zettel mit den verschlüsselten Nachrichten werden gemischt und an die Teams ausgeteilt. Es gibt zwei Gewinner:
 - Das Team, welches die erhaltene verschlüsselte Botschaft als erstes knackt und erklären kann, wie das Verfahren funktioniert. 
 - Das Team, dessen Verschlüsselung nicht geknacht werden kann (oder als letzte geknackt wird). 
5. **5'** Eine Person des Teams präsentiert ihr Verfahren an der Wandtafel.


## Regeln
 - Zum Ver- und entschlüsseln dürfen **keine** elektronische Hilfsmittel verwendet werden (keine Handys, Taschenrechner, usw.). Der Anwender muss in der Lage sein das Verfahren mit Stift und Papier verwenden zu können. 
 - Das Verfahren muss *die Zeichen der Nachricht verschlüsseln* und NICHT die Bedeutung.
 - Die verschlüsselte Botschaft muss auf einem A4 Papier platz haben (Wenn als Input ein Satz kommt). 
 - Das Verfahren muss die Zeichen A-Z und 0-9 ver- und entschlüsseln können. 