# Problematik Datenlöschungen

<sup>_(Diese Aufgabe ist für eine TEAMS-Aufgabe ausgelegt)_</sup>

Kompetenz und Lernziel siehe [Modulbaukasten](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden) (Hz 5.1)

Zeitbedarf ca. 20-40 min

Nehmen Sie mal an, es geht etwas schief beim Backup oder es wurde kein Backup gemacht, obwohl Sie glaubten, alles richtig eingestellt zu haben. Oder es macht jemand auf Ihrem Rechner oder gar auf einem Firmen-Server eine Manipulation aus Unwissenheit oder vielleicht sogar absichtlich und bösartig so, dass viel gelöscht wird.

Wenn etwas schief geht, dann geht's oft auch ganz richtig schief und es passieren mehrere Fehler nacheinander (wie es immer bei einem Unfall vorkommen kann). 

Folgende Situation (aus dem richtigen Leben). 
Jemand löscht "alles" und Sie sagen: "Kein Problem, wir haben ja ein Backup". Sie spielen das Backup zurück und müssen dann feststellen, dass dieses Backup, nicht wie draufgeschrieben vom letzten Monat ist, sondern vom letzten Monat, aber vor 10 Jahren!

Geben Sie die Antwort/en auf die diese Frage ab:
	
	"Was ist die Problematik
	von Datenlöschungen über
	alle Archive und Backups?"

Erwartet werden etwa 3-4 voll ausformulierte Sätze in deutsch oder englisch. Sie können sich in der Kleingruppe (2-3 Personen) besprechen, aber jede:r schreibt einen eigenen Text. (Bei gleichen (z.B. kopierten) Antworten wird die Bewertung 0 Punkte vergeben und es gilt wie nicht abgegeben und die Aufgabe wird zur Überarbeitung zurückgewiesen)

Die Bewertung der Punkte widerspiegelt, wie gut die Lehrperson Ihre Antwort(en) einstuft.