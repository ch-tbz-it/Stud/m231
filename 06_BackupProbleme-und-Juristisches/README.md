# Konsequenzen von Fehlern im Datenschutz und bei der Datensicherheit

Zwei Aufgaben zum Handlungsziel 5 gemäss [Moduldefinition](https://www.modulbaukasten.ch/module/231/1/de-DE?title=Datenschutz-und-Datensicherheit-anwenden)

- [Problematik von Datenlöschungen über alle Archive und Backups](problematik-datenloeschung.md)
- [Wesentliche juristische Voraussetzungen und Eigenheiten von Websites (z.B. Impressum, Disclaimer, AGBs)](impressum-disclaimer-agb.md)