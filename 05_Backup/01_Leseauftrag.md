# Einführung ins Thema Backup
| Arbeitsauftrag  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Aufgabenstellung  | Leseauftrag |
| Zeitbudget  |  1 Lektion |
| Ziele | Wie wissen was ein Backup ist und die wesentlichen Aspekte |

Informieren Sie sich im [*Herdt* Buch "Informationstechnologie Grundlagen (Stand 2021)" von Seite 216 bis 223](Herdt2021-InfGL_216-223.pdf) über die Grundlagen zum Thema Backup.

Themen:
 - Wohin sichern?
 - Datensicherungsstrategien
 - Schnittstellen

Tipp:
 - Wichtigste Punkte (Kernaussagen) auf einer Zusammenfassung festhalten.