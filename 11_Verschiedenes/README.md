# miscellaneous

[TOC]

## Optionale Inhalte
- [MFA bei Linux](./Optionale%20Inhalte/MFA%20bei%20Linux.md)
- [Absicherung von Endgeräten](./Optionale%20Inhalte/Absicherung%20von%20Endgeräten.md)
- [OpenData und Datenschutz]()

## Tools
 - https://www.parrotsec.org/download/
 - https://www.kali.org/

## Todo
 - Add OpenSource Beitrag
 - Firefox Extension for KeePass
 - Beitrag verlinken TPM Hack
 - Enigma Video einfügen
 - Installieren Sie Virenschutzprogramme, zum Beispiel Microsoft SE, Avira, oder Avast
 - Mehr Markdown