# Absicherung von Endgeräten

[TOC]

## Einleitung
Im Zusammenhang mit Datensicherheit und Datenschutz reicht es nicht, nur die Server zu schützen. Oft bieten die Clients ebenfalls Einfallstore über die Angreifer besser die Server attackieren können. In diesem Kapitel werden zwei mögliche ausgewählte Massnahmen vorgestellt und erklärt, wie diese die Datensicherheit und den Datenschutz stärken können.

## Festplattenverschlüsselung
Die Festplattenverschlüsselung haben Sie bereits in der [Übungsaufgabe zu Veracrypt](../../02_Verschlüsselung/04_Veracrypt.md) kennengelernt. Hier soll lediglich nochmals auf den Sinn und Zweck kurz eingegangen werden. Wofür braucht es eine Festplattenverschlüsselung?

Bei der Festplattenverschlüsselung geht es darum, dass alle Files verschlüsselt abgelegt werden. Diese werden nur entschlüsselt, wenn diese gelesen werden sollen. Damit befinden sich die unverschlüsselten Daten nur im Arbeitsspeicher (RAM). Sobald die Arbeit beendet wurde und die Daten zurück auf die Festplatte geschrieben werden, werden diese vor dem Schreiben wieder verschlüsselt. Das hat den grossen Vorteil, dass die Daten - sobald das Gerät nicht mehr benutzt wird - nur noch verschlüsselt vorliegen und nur nach erneuter Benutzeranmeldung wieder entschlüsselt und gelesen werden können.

Wenn das Endgerät (beispielsweise ein Laptop oder ein Smartphone) geklaut wird, dann kann der Dieb - weil ihm die Zugangsdaten fehlen - nur die verschlüsselten Daten vom Endgerät auslesen. Diese nützen ihm aber ohne den richtigen Key für die Entschlüsselung nichts.

Wichtig: wird das Gerät im Standby-Zustand geklaut, dann ist es möglich durch eine sogenannte "cold boot"-Attacke den Arbeitsspeicher auszulesen. Dies ist deshalb brisant, weil sich dort die unverschlüsselten und gerade verwendeten Daten befinden (darunter unter Umständen auch der Key für die Entschlüsselung der Festplatte). Weitere Details zur "cold boot"-Attacke können beispielsweise unter https://de.wikipedia.org/wiki/Kaltstartattacke nachgelesen werden.

## EDR & XDR
EDR steht für Endpoint Detection and Response. XDR steht für eXtended Detection and Response. Die beiden Arten sind eine Weiterentwicklung klassischer Sicherheitslösungen wie Antivirus, Firewall oder SIEM-Systeme. Die Software wird auf den Endpunkten installiert, arbeitet aber im Vergleich zu einem Antivirus nicht isoliert und nur auf dem Endpunkt. Meist ist die Software mit einer zentralen Verwaltung verbunden, die verdächtige Aktivitäten von allen Endgeräten sammelt und korreliert (ähnlich wie ein SIEM-System). Es müssen dafür aber nicht vorgängig Regeln definiert werde, sondern die Software erkennt - unter anderem dank des Einsatzes von KI - selbstständig abnormales verhalten. Die Systeme sind auch in der Lage - sobald abnormales Verhalten entdeckt wurde - die betroffenen Systeme automatisiert zu isolieren und damit die Bedrohung für mit dem Endgerät verbundenen Systemen einzudämmen.

EDR / XDR Systeme bieten im Vergleich zu herkömmlichen Lösungen ein etwas besser verbundene Sicht auf die aktuelle Sicherheitslage in den Systemen. So kann beispielsweise der Empfang einer Phishingmail, die anschliessende Installation der Malware und den Versuch Daten zu extrahieren durch die Systeme in einen logischen Zusammenhang gebracht werden.

### Weitere Details / Vertiefung
- Wikipedia-Artikel zu EDR: https://de.wikipedia.org/wiki/Endpoint_Detection_and_Response
- Wikipedia-Artikel zu XDR: https://de.wikipedia.org/wiki/Extended_Detection_and_Response
- Artikel von Microsoft, der EDR und XDR miteinander vergleicht: https://www.microsoft.com/de-de/security/business/security-101/edr-vs-xdr
- Artikel über Open-Source EDR-Tools: https://www.sangfor.com/blog/cybersecurity/5-best-open-source-edr-tools
- PDF (auf Download-Button klicken), welches sich mit der Tauglichkeit von EDR-Systemen auseinandersetzt: https://business-services.heise.de/security/bedrohungen-schwachstellen/beitrag/das-taugen-edr-loesungen-wirklich-3907
