# MFA bei Linux
| Arbeitsauftrag   |                                     |
|------------------|-------------------------------------|
| Sozialform       | Gruppenarbeit                       |
| Aufgabenstellung | MFA bei Linux aktivieren und testen |
| Zeitbudget       | 45 Minuten                          |
| Ziel             | Praktischer Einsatz von MFA         |

## Einleitung
Auch der Schutz von Servern und Clients durch eine MFA (multi factor authentication) ist ein zentrales Sicherheitselement. Bei Linux-Systemen lässt sich die MFA besonders einfach einbauen. Ziel dieser Übungsaufgabe ist es, dass Sie praktische Erfahrungen mit der MFA bei Serversystemen machen können.

## Theoretische Hintergründe
Linux-Systeme verwenden PAM (Pluggable Authentication Modules), um konfigurieren zu können für welche Dienste die User sich mit welchen Methoden authentifizieren können bzw. müssen (es lässt sich beispielsweise auch einstellen, dass nur eine Authentifizierung mit MFA erlaubt ist).

Weitere Details zu PAM können Sie in der [RedHat-Dokumentation](https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/6/html/managing_smart_cards/pluggable_authentication_modules) nachlesen.

## Vorgehen
Die Idee dieser kleinen Übungsaufgabe ist es, dass Sie ein Ubuntu-System als VM aufsetzen und das Login auf der VM via MFA (mit google authenticator) absichern. Sie benötigen dafür eine VM auf der ein Linux-Betriebssystem läuft (beispielsweise [Ubuntu 24.04 LTS](https://ubuntu.com/download/server)). Installieren Sie auf einer neuen VM ein Linux-Betriebssystem. Als Virtualisierungsumgebung können Sie beispielsweise [Hyper-V](https://learn.microsoft.com/de-de/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v) oder [VirtualBox](https://www.virtualbox.org/) verwenden.

Wenn Ihr Linux-System läuft, dann folgen Sie dem Artikel https://www.thomas-krenn.com/de/wiki/SSH-Login_mit_2-Faktor-Authentifizierung_absichern, um ein 2FA-Modul zu installieren und dieses in der PAM zu hinterlegen, dass 2FA verwendet werden soll für das Login.

**Wichtig:** Damit Sie sich nicht selbst aus dem System ausschliessen, stellen Sie sicher, dass Sie immer noch eine aktive Session auf den Server haben und zum Testen der Anpassungen am Login eine neue Session aufbauen bzw. verwenden.