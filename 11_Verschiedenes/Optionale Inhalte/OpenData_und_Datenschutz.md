# Open Data und Datenschutz

[TOC]

## Begriffsdefinition und Ursprung
Der Begriff Open Data steht für frei zur Verfügung stehende Daten. Die zur Verfügung gestellten Daten sind meist strukturiert. Es werden keine Lizenzgebühren oder ähnliches erhoben. Bei Open Data handelt es sich üblicherweise um anonymisierte Daten (z. B. Statistikdaten aus Erhebungen oder ähnliches).

Die Daten von Open Data kommen üblicherweise aus folgenden Quellen:
- **Regierung:** Der Staat wird durch den Steuerzahler finanziert. Deshalb veröffentlicht dieser Daten - beispielsweise Statistiken zu Bevölkerung - und macht diese somit nutzbar für jeden Bürger aber auch für die Unternehmen.
- **Forschungsdaten:** Hochschulen und Universitäten haben häufig ebenfalls ein Interesse Ihre Forschungsergebnisse / Forschungsdaten mit der Welt zu teilen. Einerseits kriegen sie so Inputs von anderen Forschern, die Ihre Ergebnisse / Daten überprüfen können. Andererseits ist die Forschung häufig auch durch den Steuerzahler finanziert. Die Hochschulen / Universitäten fühlen sich dadurch ein Stück weit auch verpflichtet, die Ergebnisse mit der Bevölkerung zu teilen.
- **Community-getriebene Projekte:** [Wikidata](), [Waze]() oder [OpenStreetMap]() sind einige Beispiele. Auch Wikipedia selbst ist eine Sammlung von Daten. Es wird hier aber nicht von Open Data gesprochen, weil die Daten nicht strukturiert vorliegen. Waze und OpenStreetMap sind Community-getriebene Landkarten. Jeder aus der Bevölkerung kann mithelfen die Kartendaten zu pflegen und zu erweitern.

**Wichtig:** von Open Data wird dann gesprochen, wenn die Daten strukturiert vorliegen - beispielsweise als csv/excel-Format oder auch als SQL-Dump oder ähnliches. Unstrukturierte Daten werden - selbst wenn diese frei Verfügbar sind - nicht als Open Data bezeichnet.

## Datenschutzaspekte
Open Data kann auch personenbezogene Daten enthalten, weshalb Datenschutz auch hier ein Thema ist. Wenn Open Data veröffentlicht werden soll, müssen die Daten entweder anonymisiert werden (beispielsweise bei Statistik-Daten) oder die Einstimmung der betroffenen Personen muss eingeholt werden.

Es gibt grundsätzlich keine Einschränkungen, wie die Daten verwendet werden dürfen. Wenn Open Data jedoch für die Profilbildung verwendet wird oder aus anonymisierten Daten Rückschlüsse auf konkrete Einzelpersonen gemacht werden, dann unterliegen diese Tätigkeiten aber sehr wohl dem Datenschutz - d. h. es wird die Einwilligung der von den Daten betroffenen Personen benötigt, damit diese Daten in der Art verarbeitet werden dürfen.

## Quellen und weiterführende Informationen
- https://opendata.swiss: Plattform "open government data" (Daten erhoben / bearbeitet durch die Schweizer Behörden) zur Verfügung stellt.
- https://data.stadt-zuerich.ch/: Open Data der Stadt Zürich
- https://opentransportdata.swiss: Mobilitätsdaten der Schweiz
- https://de.wikipedia.org/wiki/Open_Data: Wikipedia Artikel zu Open Data